<?php

/* Normalizes dayOfWeek to 0 - Mon, 6 - Sun
 * Add +7 because PHP mod is broken on negative numbers
 */
$date=date('d-m-Y');
$dayOfWeek = (date('w')-1 + 7) % 7;
$isWeekend = ($dayOfWeek > 4);
$dayOfWeek = min($dayOfWeek, 4);

// fetch by URL with caching
function getURL($url, bool $use_include_path = FALSE, $context = NULL) {
    $cachePrefix = 'cache/'; // cache/ is expected to be hidden with .htaccess
    $fname = $cachePrefix . preg_replace("/[^a-zA-Z0-9]/", "_", $url);
    $maxAge = 1 * 60; // 1 minute

    // Is it cached and recent?
    if (file_exists($fname) && filemtime($fname) + $maxAge > time()) {
        return file_get_contents($fname);
    }

    // Download and save to cache
    $response = file_get_contents($url, $use_include_path, $context);
    if (!is_dir($cachePrefix)) {
        mkdir($cachePrefix);
    }
    file_put_contents($fname, $response);
    return $response;
}

// include all files in places/
foreach (glob("src/places/*.php") as $filename) {
    include $filename;
}

// // bump up the daily counter
// include 'counter.php';

$places = array(
    array(
        'func' => 'malinova',
        'name' => 'Restaurace v Malinové (11:00-14:00)',
        'href' => 'http://restauracevmalinove.cz/denni-menu/',
    ),
    array(
        'func' => 'kotelna',
        'name' => 'Kotelna',
        'href' => 'https://restauracekotelna.cz/',
    ),
    array(
        'func' => 'gurmet',
        'name' => 'Gurmet 2 (otevírací doba 10:00-14:30)',
        'href' => 'https://jidelnagurmet2.cz/tydenni-menu/',
    ),
    array(
        'func' => 'allegria',
        'name' => 'Pizzeria Allegria (11:00-15:00)',
        'href' => 'https://pizzeriaallegria.cz/poledni-menu/',
    ),
    array(
        'func' => 'rozmaryna',
        'name' => 'Rozmarýna (11:00-14:00)',
        'href' => 'https://www.restauracerozmaryna.cz/denni-menu.html',
    ),
    array(
        'func' => 'usvehly',
        'name' => 'U Švehly (11:00-15:00)',
        'href' => 'https://www.u-svehly.cz/denni-menu/',
    ),
    array(
        'func' => 'pivstro',
        'name' => 'Pivstro u Petry (11:00-15:30)',
        'href' => 'https://pivstroupetry.cz/tÝdennÍ-menu/',
    ),
    array(
        'func' => 'starypan',
        'name' => 'Starý pán (11:00-15:00)',
        'href' => 'https://www.starypan.cz/index.php#DnesniNabidka',
    ),
    array(
        'func' => 'hamr',
        'name' => 'Hamr',
        'href' => 'https://www.hamrsport.cz/restaurace/restaurace-zabehlice/restaurace-zabehlice-denni-menu',
    ),
    // array(  // aktualne nemaji teplou kuchyni
    //     'func' => 'topolova',
    //     'name' => 'Rezidence Topolová',
    //     'href' => 'https://www.restauracetopolova.cz/denni-menu/',
    // ),
);
$response = array();
foreach($places as $placeArr) {
    try {
        $menu = $placeArr['func']();
        if (strlen($menu) <= 5) {
            throw new Exception('Nothing to eat');
        }
    } catch(Throwable $t) {
        $menu = 'Not available';
    }
    $response[$placeArr['func']] = array(
        'name' => $placeArr['name'],
        'href' => $placeArr['href'],
        'menu' => $menu,
    );
} 
?>

