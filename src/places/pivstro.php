<?php
function pivstro() {
    $page = new DOMDocument();
    $pageRaw = getURL('https://pivstroupetry.cz/tÝdennÍ-menu/');
    // Sanitize HTML
    $pageRaw = str_replace(array("\r", "\n"), '', $pageRaw);
    @$page->loadHTML($pageRaw);
    $content = $page->getElementById('isPasted')->C14N();

    $menuClean = $content;
    // $menuClean = preg_replace('/<\/p><p>/', "<br>", $menuClean);

    return $menuClean;
}
?>
