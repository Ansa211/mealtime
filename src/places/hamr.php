<?php
function hamr() {
    $page = new DOMDocument();
    $pageRaw = getURL('https://www.hamrsport.cz/restaurace/restaurace-zabehlice/restaurace-zabehlice-denni-menu');
    // Sanitize HTML
    $pageRaw = str_replace(array("\r", "\n"), '', $pageRaw);
    @$page->loadHTML($pageRaw);

    $content = $page->C14N();
    $menuClean = $content;
    
    $finder = new DomXPath($page);

    $mealTypes = ['Polévky', 'Hlavní jídla', 'Přílohy'];
    $menuClean = '';

    foreach ($mealTypes as $mealType) {
        // Construct the XPath query for the current meal type
        $query = "//*[@class='pageTableBlockWidget'][descendant::*[contains(text(), '$mealType')]]";
        
        // Execute the query
        $nodes = $finder->query($query);
        
        // Append the result to menuClean if nodes are found
        if ($nodes->length > 0) {
            $menuClean .= $nodes->item(0)->C14N();
        }
    }


    // $menuClean = preg_replace('/<\/p><p>/', "<br>", $menuClean);

    return $menuClean;
}
?>
