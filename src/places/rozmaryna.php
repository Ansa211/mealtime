<?php
function rozmaryna() {
    $page = new DOMDocument();
    $pageRaw = getURL('https://www.restauracerozmaryna.cz/denni-menu.html');
    // Sanitize HTML
    $pageRaw = str_replace(array("\r", "\n"), '', $pageRaw);
    @$page->loadHTML($pageRaw);
    $content = $page->getElementById('food')->C14N();

    $menuClean = $content;
    $menuClean = preg_replace('@<div class="line"[^>]*>[^<]*<\/div>@', "", $menuClean);
    $menuClean = preg_replace('/<h4>/', " ", $menuClean);
    $menuClean = preg_replace('/<\/h4>/', "", $menuClean);
    $menuClean = preg_replace('/h3>/', "h4>", $menuClean);

    return $menuClean;
}
?>
