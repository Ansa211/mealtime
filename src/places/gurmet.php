<?php
function gurmet() {
    $pageRaw = getURL('https://jidelnagurmet2.cz/tydenni-menu/');
    // Sanitize HTML
    $pageRaw = str_replace(array("\r", "\n"), '', $pageRaw);

    $page = new DOMDocument();
    @$page->loadHTML($pageRaw);

    $finder = new DomXPath($page);
    $nodes = $finder->query("//div[@id='content']//ul[1]");
    $menuClean =$nodes[0]->C14N();

    $menuClean = preg_replace('@<ul>([^<]*)<\/ul>@', "$1", $menuClean);
    $menuClean = preg_replace('@<li>([^<]*)<\/li>@', "<div>$1</div>", $menuClean);

    return $menuClean;
}
?>
