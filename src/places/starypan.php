
<?php
function starypan() {
    $date=date('d-m-Y');
    $pageRaw = getURL('https://www.starypan.cz/index.php#DnesniNabidka');
    // Sanitize HTML
    $pageRaw = str_replace(array("\r", "\n"), '', $pageRaw);

    $page = new DOMDocument();
    @$page->loadHTML($pageRaw);

    $finder = new DomXPath($page);
    $nodes = $finder->query("//div[@class='Offer']");
    $menuClean =$nodes[0]->C14N();

    $menuClean = preg_replace('@<th>@', "<td>", $menuClean, -1);
    $menuClean = preg_replace('@</th>@', "</td>", $menuClean, -1);
    //$menuClean = preg_replace('@(<\/div>)@', "$1<br>", $menuClean);

    return $menuClean;
}
?>
