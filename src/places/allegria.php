
<?php
function allegria() {
    $date=date('d-m-Y');
    $pageRaw = getURL('https://www.menicka.cz/api/iframe/?id=8913&#038;datum=dnes');
    // Sanitize HTML
    $pageRaw = str_replace(array("\r", "\n"), '', $pageRaw);

    $page = new DOMDocument();
    @$page->loadHTML($pageRaw);

    // $content = $page->getElementById('site-main')->C14N();
    //$menuClean = $content;

    $finder = new DomXPath($page);
    $nodes = $finder->query("//span[@class='dnes']//../..");
    $menuClean =$nodes[0]->C14N();

    
    $menuClean = preg_replace('@<h2[^<]*<\/h2>@', "", $menuClean);
    $menuClean = preg_replace('@<h2[^<]*<\/h2>@', "", $menuClean);
    $menuClean = preg_replace('@<h3[^<]*<\/h3>@', "", $menuClean);
    $menuClean = preg_replace('@<h4[^<]*<\/h4>@', "", $menuClean);
    $menuClean = preg_replace('/\s*<br>\s*/', "&nbsp;&nbsp;&nbsp;", $menuClean, -1);
    $menuClean = preg_replace('@<.br>@', "", $menuClean, -1);  // note the extra character between < and br !!!
    $menuClean = preg_replace('@a *([1-9][0-9,\s]*) *@', "&nbsp;&nbsp;&nbsp;$1", $menuClean);
    $menuClean = preg_replace('@div@', 'span', $menuClean);
    $menuClean = preg_replace('@class="col-md-\d* *@', 'class="', $menuClean);

    return $menuClean;
}
?>
