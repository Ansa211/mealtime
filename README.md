# Mealtime
Inspired by
[cfm.vilda.net](https://cfm.vilda.net)
([github](https://github.com/zouharvi/cfm)),
Mealtime fetches daily menus of restaurants around Zahradní město.

## Development

### Requirements
`php, php-cli, php-common, php-fpm, php-json, php-xml` version `>=7.3.0` should be enough.

### Local development
```
php -S localhost:8000
google-chrome http://localhost:8000/index.php
```



