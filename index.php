<html>
<head>
    <title>Mealtime</title>
    <link rel="shortcut icon" href="https://cdn-icons-png.flaticon.com/512/6413/6413267.png" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400" media="all">
    <style>
        * { font-family: Roboto; }
        a { text-decoration: none; }
        .food-price { float: right; }
        .text-right { float: right; }
        .podtr { width: 100%; display: block; align-items: flex-end; flex-wrap: wrap }
    </style>
</head>

<body style='background-color: #f4f4f4'>
    <h1>It's mealtime!</h1>
    <?php
    include 'src/raw.php';
    
    if ($isWeekend) {
        echo "<p>It's weekend now, so I'm showing last available lunch menus.</p>";
    }

    foreach($response as $place) {
        echo '<h3 style="background-color: yellow"><a href="' . $place['href'] . '">' . $place['name'] . '</a></h3>';
        $menu = $place['menu'];
        $menu = str_replace("\n", '<br>', $menu);
        echo '<p>' . $menu . '</p>';
    }
    ?>

    <h3 style="background-color: yellow">Další restaurace v okolí</h3>
    <div><strong>Čína, sushi: </strong> Cheng De v OC Cíl, Jiang Jia ve Zvonkové, Asia restaurant u Květu</div>
    <div><strong>Kebab, pizza a fast-food: </strong> MaMa kebab &amp; pizza (u zastávky CZM), <a href="https://www.foodora.cz/restaurant/wkba/svehlovka-bistro">Bistro Švehlovka</a>, Pizza Joy u zastávky Záběhlická škola, El Kefi Kebab u Květu</div>
    <div><strong>Cukrárny a kavárny: </strong> Zelenkova cukrárna (u Cíle), Jasmínka, Kafe Květ</div>
    <div><strong>Vivo: </strong> mexická, wok, sushi, čínská, česká kuchyně</div>

    <br>
    <div>
        Mealtime is Anša's fork of the CfM project, made by Vilda with the help of others from MS.
    </div>
    
    <div style='font-weight: bold;'>
        <a href="https://gitlab.com/Ansa211/mealtime">Gitlab</a><!--, <a href="statistics.php">Statistics</a>-->
    </div>
</body>
</html>
